# PlayStation 60 fps cheats

This repository contains 60 fps cheats for PSX and PS2 games.

The files are named based on the title of the game and its corresponding serial in order to make it easier to identify and import the files to your emulator.

Original resources:
- PSX games: [Google Docs code compilation](https://docs.google.com/spreadsheets/d/1BoN2X_tebIfaqTFC_XUYuazPSY_fMrDUE7NI5psw0LE/edit#gid=0)
- PS2 games: [PCSX2 forum](https://forums.pcsx2.net/Thread-60-fps-codes)

Tooling: the codes themselves are stored in `.yml` files named after the corresponding platform, and a Python 3 script is used to generate the cheats.

