import re
import yaml

def generate_files_psx(db_file):
    for i in db_file:
        outfile=(open('psx/'+i['title']+'_'+i['serial']+'.cht','w'))
        outfile.write('gametitle='+i['title']+'\n')
        outfile.write('// 60 fps'+'\n')
        for j in i['trickline']:
            outfile.write(j+'\n')
        outfile.close()


def generate_files_ps2(db_file):
    for i in db_file:
        outfile=(open('ps2/'+i['title']+'_'+i['serial']+'.pnach','w'))
        outfile.write('gametitle='+i['title']+' '+i['serial']+'\n')
        outfile.write('comment=60 fps'+'\n')
        for j in i['trickline']:
            code0=j[:8]
            code1=j[:17][-8:]
            comment=j[17:]
            outfile.write('patch=1,EE,'+code0+',extended,'+code1+'\n')
        outfile.close()


def generate_cheatdb(outfile,data):
    stream = open(outfile, 'w')
    yaml.dump(data, stream)

def main():
    psx_db = yaml.load(open('psx.yml', 'r'),Loader=yaml.FullLoader)
    generate_files_psx(psx_db)
    #generate_cheatdb('psx.yml',psx_db)
    
    ps2_db = yaml.load(open('ps2.yml', 'r'),Loader=yaml.FullLoader)
    generate_files_ps2(ps2_db)
    #generate_cheatdb('ps2.yml',ps2_db)
    
main()